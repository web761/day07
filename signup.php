<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->
    <style>
        .form-group input[type="text" i] {
            border: 2px solid #4d7aa2;
        }
    </style>
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div id="main">

        <div class="wrapper">

            <div class="showError">
                <?php
                $nameErr = $falcutyErr = $genderErr = $dobErr = $addressErr = "";
                $name = $falcuty = $gender = $dob = $address = "";
                session_start();
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    // Validate
                    if (empty($_POST["name"])) {
                        $nameErr = "Hãy nhập họ tên";
                        echo '<p style="color:red;">' . $nameErr . '</p>';
                    } else {
                        $name = test_input($_POST["name"]);
                    }
                    if (empty($_POST["falcuty"]) || $_POST["falcuty"] == 'None') {
                        $falcutyErr = "Hãy nhập tên khoa";
                        echo '<p style="color:red;">' . $falcutyErr . '</p>';
                    } else {
                        $falcuty = test_input($_POST["falcuty"]);
                    }

                    if (empty($_POST["gender"])) {
                        $genderErr = "Hãy nhập giới tính";
                        echo '<p style="color:red;">' . $genderErr . '</p>';
                    } else {
                        $gender = test_input($_POST["gender"]);
                    }

                    if (empty($_POST["dob"])) {
                        $dobErr = "Hãy nhập ngày sinh";
                        echo '<p style="color:red;">' . $dobErr . '</p>';
                    } else {
                        $dob = test_input($_POST["dob"]);
                    }

                    if (empty($_POST["address"])) {
                        $addressErr = "Hãy nhập địa chỉ";
                        echo '<p style="color:red;">' . $addressErr . '</p>';
                    } else {
                        $address = test_input($_POST["address"]);
                    }

                    //upload file 

                    $allowUpload   = true;
                    $targetdir    = "upload/";
                    if (!is_dir($targetdir)) {
                        mkdir($targetdir);
                    }
                    if (!empty($_FILES['image']["name"])) {
                        date_default_timezone_set('Asia/Ho_Chi_Minh');
                        $target_file   = $targetdir . basename($_FILES["image"]["name"]);
                        $fileName = pathinfo($_FILES["image"]["name"]);
                        $savedFileName = $targetdir . $fileName["filename"] . '_' . date("YmdHis") . '.' . $fileName["extension"];


                        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                        $allowtypes    = array('jpg', 'png', 'jpeg');
                        if (!in_array($imageFileType, $allowtypes)) {
                            echo '<p class="errorMsg">Chỉ upload ảnh định dạng png, jpg, jpeg</p>';
                            $allowUpload = false;
                        }

                        if (!$_FILES["image"]["error"]) {
                            move_uploaded_file($_FILES["image"]["tmp_name"], $savedFileName);
                        } else {
                            echo '<p class="errorMsg">' . var_dump($_FILES["image"]) . '</p>';
                            $allowUpload = false;
                        }
                    } else {
                        $allowUpload = true;
                    }

                    if (empty($addressErr) && empty($dobErr) && empty($falcutyErr) && empty($dobErr) && empty($nameErr) && $allowUpload) {
                        // $_POST['file'] = $_FILES;
                        $_SESSION = $_POST;
                        $_SESSION["image"] = $savedFileName;
                        header("location: submit_form.php");
                    }
                   
                }

                function test_input($data)
                {
                    $data = trim($data);
                    $data = stripslashes($data);
                    $data = htmlspecialchars($data);
                    return $data;
                }
                ?>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
                <?php echo '<div class="form-group">
                    <label class="form-label">Họ và tên <span class="required">*</span></label></label>
                    <input class="name" type="text" name="name" id="">    
                </div>' ?>

                <div class="form-group">
                <label class="form-label">Giới tính <span class="required">*</span></label></label>
                <?php
                $sex = array("Nam","Nữ");
                for ($i = 0; $i < count($sex); $i++) {
                    echo "
                            <input class=\"input_sex\" name = \"sex\" type=\"radio\" value=\"$i\">{$sex[$i]}
                        ";
                }
                $_SESSION['sex_array'] = $sex;
                ?>
                </div>
                <div class="form-group">
                    <label class="form-label">Phân khoa <span class="required">*</span></label></label>
                    <select name="falcuty" id="falcuties">
                        <?php
                            $falcuty = array("SPA" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                            foreach ($falcuty as $key => $value) {
                                echo '<option value="' . $key . '">' . $value . '</option>';
                            }
                            $_SESSION['department_array'] = $falcuty;
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-label">Ngày sinh <span class="required">*</span></label>
                    <input id="txtDate" type="text" name="dob" placeholder="dd/mm/yyyy" style="width: 246px;">
                </div>
                <?php echo '<div class="form-group">
                    <label class="form-label">Địa chỉ</label>
                    <input class="name" type="text" name="address" id="">    
                </div>' ?>

                <div class="form-group" >
                    <label class="form-label" style="width: 32%;">Hình ảnh</label>
                    <input type="file" name="image" id="image" style="margin-right:-12px;" onchange="readURL(this);">
    
                </div>
                <!-- <input class="img_encode" name="img_encode" style="display: none;" /> -->

                <button class="submit-btn" type="submit">Đăng ký</button>

            </form>
        </div>
    </div>
    <Script>
        function readURL(input) {
            console.log(input.files[0]);
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                $('#blah').css("display", "block");
                reader.onload = function(e) {
                    $('#blah')
                        .attr('src', e.target.result);

                    $('.img_encode').val(e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            } else {
                $('#blah').css("display", "none");
            }
        }
    </Script>


    <!-- Bootstrap -->
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
        $(function() {
            $('#txtDate').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>
</body>

</html>