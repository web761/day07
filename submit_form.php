<?php
    session_start();
    // $img = $_SESSION["img_encode"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <div id="main">

        <div class="wrapper">

            <form action="" method="">

                <div class="form-group">
                    <label class="form-label">Họ và tên </label></label>
                    <?php
                        print_r($_SESSION['name'])
                    ?>
                </div>

                <div class="form-group">
                <label class="form-label">Giới tính</label></label>
                <?php
                    print_r($_SESSION['sex_array'][$_SESSION['sex']])
                ?>
                </div>

                <div class="form-group">
                    <label class="form-label">Phân khoa </label></label>
                    <?php
                    print_r($_SESSION['department_array'][$_SESSION['falcuty']])
                    ?>
                </div>

                <div class="form-group">
                    <label class="form-label">Ngày sinh </label>
                    <?php
                    print_r($_SESSION['dob'])
                    ?>
                </div>

                <div class="form-group">
                    <label class="form-label">Địa chỉ</label>
                    <?php
                    if ($_SESSION['address'] == null) {
                        echo "";
                    } else 
                        print_r($_SESSION['address']);
                    ?>   
                </div>

               

                <div class="form-group">
                    <label class="form-label">Hình ảnh</label>
                    <div class="form-input">
                        <?php
                        if (!empty($_SESSION["image"])) {
                            echo '<img class="studentImg" src="' . $_SESSION["image"] . '" alt="Student avatar">';
                        } ?>
                    </div>

                </div>


                <button class="submit-btn" type="submit">Xác nhận</button>

            </form>
        </div>
    </div>

    <!-- Bootstrap -->
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
        $(function() {
            $('#txtDate').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>

</body>
</html>